/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rmiserver;

import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

/**
 * RMI Server
 * @author Patrick P. (<a href="http://ppati000.tk/">ppati000.tk</a> or <a href="mailto:ppati.email@gmail.com">ppati.email@gmail.com</a>)
 * @version 0.1
 */
public class RMIServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            
            /*Ein Objekt der Klasse Server erzeugen, dessen Methoden wir später
            vom Client aufrufen werden*/
            Server serverObjekt = new ServerImpl();
            //Macht das Objekt verfügbar für Aufrufe auf dem Standard-Port und generiert Stub
            Server serverStub = (Server) UnicastRemoteObject.exportObject(serverObjekt, 0);
            
            /*Erzeugt eine Registry auf Port 1099, und registriert das Server-
            Objekt unter "server"*/
            LocateRegistry.createRegistry(1099);
            LocateRegistry.getRegistry(1099).bind("server", serverStub);
            
            System.out.println("Server ist bereit!");
        } catch (AccessException ex) {
            System.err.println("Keine Berechtigung zum Zugriff :(");
            ex.printStackTrace();
        } catch (AlreadyBoundException ex) {
            System.err.println("Objekt mit diesem Namen ist schon gebunden!");
            ex.printStackTrace();
        }
        catch (RemoteException ex) {
            System.err.println("Fehler beim Export des Server-Objekts :(");
            ex.printStackTrace();
        }
        
    }

}
